<?php
return array(
	'CONFIG_VERSION' => '1.90',
	'database_Location' => '/config/organizr/',
	'timezone' => 'Australia/Sydney',
	'user_home' => '/config/organizr/users/',
);